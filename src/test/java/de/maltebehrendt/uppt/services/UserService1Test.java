package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.InstanceRunner;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.codec.binary.Base64;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by malte on 01.02.17.
 */
@RunWith(VertxUnitRunner.class)
public class UserService1Test {
    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    private static String configDirectory = "test" + File.separator + "userServiceTest" + File.separator + "inMemory" + File.separator + "config" + File.separator;

    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(new JsonArray().add("userService1.json"));
        eventBus = vertx.eventBus();

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));
                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testNewEMailUserRoundtrip(TestContext context) {
        Async requestResult = context.async(5);

        JsonObject newEMailUser = new JsonObject()
                .put("email", "test@test.de")
                .put("passwordKeyFactory", "PBKDF2WithHmacSHA512")
                .put("passwordSalt", "testSaltToping")
                .put("passwordIterations", 10)
                .put("passwordKeyLength", 256)
                .put("roles", new JsonArray().add("tester").add("superTester"));

        String password = "superSecret12$%&\\234?.;.°";
        String passwordHash = hashPassword(password,
                newEMailUser.getString("passwordKeyFactory"),
                newEMailUser.getString("passwordSalt"),
                newEMailUser.getInteger("passwordIterations"),
                newEMailUser.getInteger("passwordKeyLength"));
        newEMailUser.put("passwordHash", passwordHash);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "456456")
                .addHeader("origin", "UserService1Test");

        eventBus.send("users.1.newEMailUser", newEMailUser, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for creating new email user");
            context.assertEquals("200", reply.result().headers().get("statusCode"), "New email user has been created");
            requestResult.countDown();

            eventBus.send("users.1.userBasicsUnknown",new JsonObject().put("id", newEMailUser.getString("email")), options, basicsReply -> {
                context.assertTrue(basicsReply.succeeded(), "Received reply for retrieving new email user basic info");
                context.assertEquals("200", basicsReply.result().headers().get("statusCode"), "New email user has been found");
                JsonObject userBasics = (JsonObject) basicsReply.result().body();
                context.assertEquals(newEMailUser.getString("email"), userBasics.getString("id"), "Email is set as ID");
                context.assertEquals(newEMailUser.getJsonArray("roles").size(), userBasics.getJsonArray("roles").size(), "Number of roles matches");
                context.assertEquals(newEMailUser.getJsonArray("roles"), userBasics.getJsonArray("roles"), "Roles were set");
                requestResult.countDown();


                eventBus.send("users.1.missingPBEKeySpecForEMailUser",new JsonObject().put("email", newEMailUser.getString("email")), options, specsReply -> {
                    context.assertTrue(specsReply.succeeded(), "Received reply for missing user hashing specs");
                    context.assertEquals("200", specsReply.result().headers().get("statusCode"), "Email user's hashing spec has been found");
                    JsonObject hashingSpec = (JsonObject) specsReply.result().body();
                    context.assertEquals(newEMailUser.getString("passwordKeyFactory"), hashingSpec.getString("passwordKeyFactory"), "passwordKeyFactory is set correctly");
                    context.assertEquals(newEMailUser.getString("passwordSalt"), hashingSpec.getString("passwordSalt"), "passwordSalt is set correctly");
                    context.assertEquals(newEMailUser.getInteger("passwordIterations"), hashingSpec.getInteger("passwordIterations"), "passwordIterations is set correctly");
                    context.assertEquals(newEMailUser.getInteger("passwordKeyLength"), hashingSpec.getInteger("passwordKeyLength"), "passwordKeyLength is set correctly");

                    String newPasswordHash = hashPassword(password,
                            hashingSpec.getString("passwordKeyFactory"),
                            hashingSpec.getString("passwordSalt"),
                            hashingSpec.getInteger("passwordIterations"),
                            hashingSpec.getInteger("passwordKeyLength"));
                    context.assertEquals(newPasswordHash, passwordHash, "Password has can be replicated from saved specs and password");

                    requestResult.countDown();

                    eventBus.send("users.1.userBecameObsolete",new JsonObject().put("id", newEMailUser.getString("email")), options, delReply -> {
                        context.assertTrue(delReply.succeeded(), "Received reply for obsolete user");
                        context.assertEquals("200", delReply.result().headers().get("statusCode"), "User has been deleted");
                        requestResult.countDown();

                        eventBus.send("users.1.userBasicsUnknown",new JsonObject().put("id", newEMailUser.getString("email")), options, checkReply -> {
                            context.assertTrue(checkReply.succeeded(), "Received reply for retrieving deleted user basic info");
                            context.assertEquals("404", checkReply.result().headers().get("statusCode"), "Deleted user has not been found");
                            requestResult.countDown();
                        });
                    });
                });
            });
        });

        requestResult.awaitSuccess();
    }

    private static String hashPassword(final String password, final String keyFactory, final String saltString, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( keyFactory );
            byte[] salt = saltString.getBytes("UTF-8");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return new Base64().encodeAsString(res);

        } catch( NoSuchAlgorithmException | InvalidKeySpecException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void tearDownVertx(TestContext context) {
        try {
            vertx.close();
        }
        catch(Exception e) {}
    }
}
