package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by malte on 31.01.17.
 */
public class UserService1 extends AbstractService {
    private ConcurrentHashMap<String, JsonObject> userData = null;
    private MessageDigest messageDigest = null;

    // TODO: THIS IS ONLY A WORK AROUND FOR NOW!
    // TODO: logging!

    @Override
    public void prepare(Future<Object> prepareFuture) {
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.fatal("Required message digest SHA-256 is not supported!");
        }
        // get a connection to the user storage
        switch(config().getString("storageBackend")) {
            case "localInMemory":
                userData = new ConcurrentHashMap<>();
                break;
            default:
                logger.fatal("Required storage backend " + config().getString("storageBackend") + " is not supported!");
        }

        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {

    }


    @Processor(
            domain = "users",
            version = "1",
            type = "newEMailUser",
            description = "A new user account with email credentials is required",
            requires = {
                    @Payload(key = "email", type = DataType.STRING, description = "EMail address of the user"),
                    @Payload(key = "passwordHash", type = DataType.STRING, description = "Hash of the password"),
                    @Payload(key = "passwordKeyFactory", type = DataType.STRING, description = "KeyFactory used to hash the password"),
                    @Payload(key = "passwordSalt", type = DataType.STRING, description = "Salt used to hash the password"),
                    @Payload(key = "passwordIterations", type = DataType.STRING, description = "Iterations used to hash the password"),
                    @Payload(key = "passwordKeyLength", type = DataType.STRING, description = "Keylength used to hash the password"),
                    @Payload(key = "roles", type = DataType.JSONArray, description = "Roles the user has")
            }
    )
    public void processNewEMailUser(Message message) {
        JsonObject user = message.getBodyAsJsonObject();
        user.put("id", user.getString("email"));
        userData.put(message.getBodyAsJsonObject().getString("id"), user);
        message.reply(200);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "newSessionInformation",
            description = "Stores session information",
            requires = {
                    @Payload(key = "id", type = DataType.STRING, description = "ID of the user"),
                    @Payload(key = "sessionSource", type = DataType.STRING, description = "Remote IP address, from which the session was initiated"),
                    @Payload(key = "sessionStartTimestamp", type = DataType.LONG, description = "Timestamp of session creation"),
                    @Payload(key = "sessionAddress", type = DataType.STRING, description = "EventBus address for sending/publishing to the user/session"),
                    @Payload(key = "sessionToken", type = DataType.STRING, description = "Token of the session")
            }
    )
    public void processNewSessionInformation(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();

        JsonObject sessionInformation = new JsonObject()
                .put("sessionSource", messageBody.getString("sessionSource"))
                .put("sessionStartTimestamp", messageBody.getLong("sessionStartTimestamp"))
                .put("sessionAddress", messageBody.getString("sessionToken"));

        JsonObject user = userData.get(messageBody.getString("id"));

        if(user == null) {
            message.reply(404);
            return;
        }
        if(!user.containsKey("sessions")) {
            user.put("sessions", new JsonObject());
        }
        user.getJsonObject("sessions").put(messageBody.getString("sessionToken"), sessionInformation);
        message.reply(200);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "endSessionInformation",
            description = "Stores session end information",
            requires = {
                    @Payload(key = "id", type = DataType.STRING, description = "ID of the user"),
                    @Payload(key = "sessionEndTimestamp", type = DataType.LONG, description = "Timestamp of session closing"),
                    @Payload(key = "sessionToken", type = DataType.STRING, description = "Token of the session")
            }
    )
    public void processEndSessionInformation(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();
        JsonObject user = userData.get(messageBody.getString("id"));

        if(user == null) {
            message.reply(404);
            return;
        }
        if(!user.containsKey("sessions") || !user.getJsonObject("sessions").containsKey(messageBody.getString("sessionToken"))) {
            message.reply(404);
            return;
        }
        user.getJsonObject("sessions").getJsonObject(messageBody.getString("sessionToken")).put("sessionEndTimestamp",messageBody.getLong("sessionEndTimestamp"));
        message.reply(200);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "missingSessionInformation",
            description = "Returns session information",
            requires = {
                    @Payload(key = "id", type = DataType.STRING, description = "ID of the user"),
                    @Payload(key = "sessionToken", type = DataType.STRING, description = "Token of the session")
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user"),
                    @Payload(key = "userRoles", type = DataType.JSONArray, description = "Array of all the user has"),
                    @Payload(key = "sessionSource", type = DataType.STRING, description = "Remote IP address, from which the session was initiated"),
                    @Payload(key = "sessionStartTimestamp", type = DataType.LONG, description = "Timestamp of session creation"),
                    @Payload(key = "sessionEndTimestamp", type = DataType.LONG, description = "Timestamp of session closing"),
                    @Payload(key = "sessionAddress", type = DataType.STRING, description = "EventBus address for sending/publishing to the user/session")
            }
    )
    public void processMissingSessionInformation(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();

        JsonObject user = userData.get(messageBody.getString("id"));

        if(user == null || !user.containsKey("sessions") || !user.getJsonObject("sessions").containsKey(messageBody.getString("sessionToken"))) {
            message.reply(404);
            return;
        }

        JsonObject storedSessionInformation = user.getJsonObject("sessions").getJsonObject(messageBody.getString("sessionToken"));

        JsonObject sessionInformation = new JsonObject()
                .put("userID", messageBody.getString("id"))
                .put("userRoles", user.getJsonArray("roles"))
                .put("sessionSource", storedSessionInformation.getString("sessionSource"))
                .put("sessionStartTimestamp", storedSessionInformation.getLong("sessionSource"))
                .put("sessionEndTimestamp", storedSessionInformation.getLong("sessionEndTimestamp"))
                .put("sessionAddress", storedSessionInformation.getString("sessionAddress"));

        message.reply(sessionInformation);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "userBasicsUnknown",
            description = "A user is unknown, basic information are required",
            requires = {
                    @Payload(key = "id", type = DataType.STRING, description = "id of the user")
            },
            provides = {
                    @Payload(key = "id", type = DataType.STRING, description = "Some kind of user id"),
                    @Payload(key = "roles", type = DataType.JSONArray, description = "Roles the user has")
            }
    )
    public void processUserBasicsUnknown(Message message) {
        String userId = message.getBodyAsJsonObject().getString("id");
        JsonObject user = userData.get(userId);

        if(user == null) {
            message.reply(404);
        }
        else {
            message.reply(new JsonObject().put("id", userId).put("roles", user.getJsonArray("roles")));
        }
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "unauthenticatedEMailUser",
            description = "A authorization of a user with email credentials is required",
            requires = {
                    @Payload(key = "email", type = DataType.STRING, description = "EMail address of the user"),
                    @Payload(key = "passwordHash", type = DataType.STRING, description = "Hash of the password"),
                    @Payload(key = "remoteAddress", type = DataType.STRING, description = "Remote address of the user")
            },
            provides = {
                    @Payload(key = "id", type = DataType.STRING, description = "Some kind of user id"),
                    @Payload(key = "token", type = DataType.STRING, description = "Token proving the successful authorization"),
                    @Payload(key = "remoteAddress", type = DataType.STRING, description = "Remote address of the user"),
                    @Payload(key = "timestamp", type = DataType.DATELONG, description = "Time when the token was created"),
                    @Payload(key = "email", type = DataType.STRING, description = "EMail address of the user"),
                    @Payload(key = "roles", type = DataType.JSONArray, description = "Roles the user has")
            }
    )
    public void processEMailUserAuthentification(Message message) {
        JsonObject authInfo = message.getBodyAsJsonObject();
        JsonObject user = userData.get(authInfo.getString("email"));

        try {
            if(user == null) {
                message.reply(404);
                return;
            }
            if(user.getString("passwordHash").equals(authInfo.getString("passwordHash"))) {
                Date currentTime = new Date();

                JsonObject replyMessage = new JsonObject()
                        .put("remoteAddress", authInfo.getString("remoteAddress"))
                        .put("email", user.getString("email"))
                        .put("id", user.getString("id"))
                        .put("roles", user.getJsonArray("roles"))
                        .put("timestamp", currentTime.getTime());

                // it does not really matter what the token is, but it must be depend on time and remote address, so that replay/hijacking attacks get complicated
                byte[] digest = messageDigest.digest((authInfo.getString("email") + authInfo.getString("remoteAddress") + currentTime.toString()).getBytes("UTF-8"));
                replyMessage.put("token", new Base64().encodeAsString(digest));
                message.reply(200, replyMessage);
                return;
            }
        } catch (UnsupportedEncodingException e) {
            message.reply(500, new JsonObject().put("message", e.getMessage()));
        }

        message.reply(401);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "missingPBEKeySpecForEMailUser",
            description = "Specifications for a user's password hash calculation are necessary",
            requires = {
                    @Payload(key = "email", type = DataType.STRING, description = "EMail address of the user")
            },
            provides = {
                    @Payload(key = "email", type = DataType.STRING, description = "EMail address of the user"),
                    @Payload(key = "passwordKeyFactory", type = DataType.STRING, description = "KeyFactory used to hash the password"),
                    @Payload(key = "passwordSalt", type = DataType.STRING, description = "Salt used to hash the password"),
                    @Payload(key = "passwordIterations", type = DataType.INTEGER, description = "Iterations used to hash the password"),
                    @Payload(key = "passwordKeyLength", type = DataType.INTEGER, description = "Keylength used to hash the password")
            }
    )
    public void processMissingPBEKeySpecForEMailUser(Message message) {
        JsonObject user = userData.get(message.getBodyAsJsonObject().getString("email"));

        if (user == null) {
            message.reply(404);
            return;
        }
        JsonObject replyMessage = new JsonObject()
                .put("email", user.getString("email"))
                .put("passwordKeyFactory", user.getString("passwordKeyFactory"))
                .put("passwordSalt", user.getString("passwordSalt"))
                .put("passwordIterations", user.getInteger("passwordIterations"))
                .put("passwordKeyLength", user.getInteger("passwordKeyLength"));

        message.reply(200, replyMessage);
    }

    @Processor(
            domain = "users",
            version = "1",
            type = "userBecameObsolete",
            description = "Deletes the obsolete user",
            requires = {
                    @Payload(key = "id", type = DataType.STRING, description = "ID of the user")
            }
    )
    public void processObsoleteUser(Message message) {
        userData.remove(message.getBodyAsJsonObject().getString("id"));
        message.reply(200);
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }

}
